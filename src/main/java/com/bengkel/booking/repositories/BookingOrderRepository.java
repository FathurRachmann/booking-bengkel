package com.bengkel.booking.repositories;

import java.util.ArrayList;
import java.util.List;

import com.bengkel.booking.models.BookingOrder;

public class BookingOrderRepository {
    private static List<BookingOrder> bookingOrders = new ArrayList<>();

    public static void addBookingOrder(BookingOrder bookingOrder) {
        bookingOrders.add(bookingOrder);
    }

    public static List<BookingOrder> getBookingOrdersByCustomerId(String customerId) {
        List<BookingOrder> customerBookingOrders = new ArrayList<>();
        for (BookingOrder bookingOrder : bookingOrders) {
            if (bookingOrder.getCustomerId().equals(customerId)) {
                customerBookingOrders.add(bookingOrder);
            }
        }
        return customerBookingOrders;
    }

    public static void saveBookingOrder(BookingOrder bookingOrder) {
        addBookingOrder(bookingOrder);
    }
}
