package com.bengkel.booking.models;

import java.util.Date;
import java.util.List;
import com.bengkel.booking.interfaces.IBengkelPayment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookingOrder implements IBengkelPayment {
    private String bookingId;
    private Customer customer;
    private List<ItemService> services;
    private String paymentMethod;
    private double totalServicePrice;
    private double totalPayment;
    private static final double RATES_DISCOUNT_SALDO_COIN = 0.1;
    private static final double RATES_DISCOUNT_CASH = 0.05;

    @Override
    public void calculatePayment() {
        double discount = 0;
        if (paymentMethod.equalsIgnoreCase("Saldo Coin")) {
            discount = getTotalServicePrice() * RATES_DISCOUNT_SALDO_COIN;
        } else {
            discount = getTotalServicePrice() * RATES_DISCOUNT_CASH;
        }

        setTotalPayment(getTotalServicePrice() - discount);
    }

    public String getCustomerId() {
        return customer.getCustomerId();
    }

    public Date getBookingDate() {
        return new Date();
    }

    public String getCustomerName() {
        return customer.getName();
    }

    public String getServices() {
        return formatServiceList(services);
    }

    private String formatServiceList(List<ItemService> services) {
        StringBuilder sb = new StringBuilder();
        for (ItemService service : services) {
            sb.append(service.getServiceName()).append(", ");
        }
        return sb.substring(0, sb.length() - 2); 
    }
    

    public String getServiceNames() {
        StringBuilder sb = new StringBuilder();
        for (ItemService service : services) {
            sb.append(service.getServiceName()).append(", ");
        }
        return sb.substring(0, sb.length() - 2); 
    }
}
