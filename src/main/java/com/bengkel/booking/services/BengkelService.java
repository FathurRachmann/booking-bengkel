package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;
import com.bengkel.booking.repositories.BookingOrderRepository;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class BengkelService {
    private static List<Customer> listAllCustomers = CustomerRepository.getAllCustomer();
    private static List<ItemService> listAllItemService = ItemServiceRepository.getAllItemService();
    private static Scanner input = new Scanner(System.in);
    private BookingOrderRepository bookingOrderRepository;

    private static Customer findCustomerById(String customerId) {
        for (Customer customer : listAllCustomers) {
            if (customer.getCustomerId().equals(customerId)) {
                return customer;
            }
        }
        return null; 
    }

    public static void informasiCustomer(Customer loggedInCustomer) {
        System.out.println("=== Informasi Customer ===");
        System.out.println("Customer ID: " + loggedInCustomer.getCustomerId());
        System.out.println("Nama: " + loggedInCustomer.getName());
        System.out.println("Customer Status: " + (loggedInCustomer instanceof MemberCustomer ? "Member" : "Non-Member"));
        System.out.println("Alamat: " + loggedInCustomer.getAddress());
        if (loggedInCustomer instanceof MemberCustomer) {
            System.out.println("Saldo Koin: " + ((MemberCustomer) loggedInCustomer).getSaldoCoin());
        }
        PrintService.printVechicle(loggedInCustomer.getVehicles());
        
    }

    public static void booking(Customer loggedInCustomer) {
        System.out.println("=== Booking Bengkel ===");
        System.out.print("Masukkan Vehicle ID: ");
        String vehicleId = input.nextLine();

        Vehicle vehicle = findVehicleById(loggedInCustomer, vehicleId);
        if (vehicle == null) {
            System.out.println("Kendaraan tidak ditemukan.");
            return;
        }

        displayAvailableServices(vehicle);
        List<ItemService> selectedServices = chooseServices(loggedInCustomer);
        if (selectedServices.isEmpty()) {
            System.out.println("Anda tidak memilih layanan.");
            return;
        }

        int totalCost = calculateTotalCost(selectedServices);
        System.out.println("Total Biaya: " + totalCost);

        selectPaymentMethod(loggedInCustomer, selectedServices, totalCost);
    }

    private static Vehicle findVehicleById(Customer loggedInCustomer, String vehicleId) {
        for (Vehicle vehicle : loggedInCustomer.getVehicles()) {
            if (vehicle.getVehiclesId().equals(vehicleId)) {
                return vehicle;
            }
        }
        return null; 
    }

    private static void displayAvailableServices(Vehicle vehicle) {
        System.out.println("Layanan yang tersedia untuk kendaraan:");
        PrintService.printItemService(listAllItemService, vehicle);
    }

    private static List<ItemService> chooseServices(Customer loggedInCustomer) {
        int maxServiceCount = (loggedInCustomer instanceof MemberCustomer) ? 2 : 1;
        System.out.println("Pilih layanan (maksimal " + maxServiceCount + "):");
        List<ItemService> selectedServices = new ArrayList<>();

        for (int i = 0; i < maxServiceCount; i++) {
            System.out.print("Layanan ke-" + (i + 1) + ": ");
            String serviceId = input.nextLine();
            ItemService service = findServiceById(serviceId);
            if (service != null) {
                selectedServices.add(service);
            } else {
                System.out.println("Layanan dengan ID tersebut tidak ditemukan.");
                break;
            }
        }

        return selectedServices;
    }

    private static int calculateTotalCost(List<ItemService> selectedServices) {
        int totalCost = 0;
        for (ItemService service : selectedServices) {
            totalCost += service.getPrice();
        }
        return totalCost;
    }

    private static void selectPaymentMethod(Customer loggedInCustomer, List<ItemService> selectedServices, int totalCost) {
        System.out.println("Pilih metode pembayaran:");
        System.out.println("1. Cash");
        if (loggedInCustomer instanceof MemberCustomer) {
            System.out.println("2. Saldo Coin");
        }

        int paymentChoice = Validation.validasiNumberWithRange("Masukkan Pilihan Metode Pembayaran:", "Input Harus Berupa Angka!", "^[0-9]+$", 2, 1);

        switch (paymentChoice) {
            case 1:
                processCashPayment(loggedInCustomer, selectedServices, totalCost);
                break;
            case 2:
                if (loggedInCustomer instanceof MemberCustomer) {
                    processCoinPayment((MemberCustomer) loggedInCustomer, selectedServices, totalCost);
                } else {
                    System.out.println("Pilihan metode pembayaran tidak valid.");
                }
                break;
            default:
                System.out.println("Pilihan metode pembayaran tidak valid.");
                break;
        }
    }

    private static void processCashPayment(Customer loggedInCustomer, List<ItemService> selectedServices, int totalCost) {
        BookingOrder bookingOrder = new BookingOrder();
        bookingOrder.setBookingId(generateUniqueBookingId()); 
        bookingOrder.setCustomer(loggedInCustomer);
        bookingOrder.setServices(selectedServices);
        bookingOrder.setPaymentMethod("Cash");
        bookingOrder.setTotalServicePrice(totalCost);
        bookingOrder.calculatePayment(); 
    
        BookingOrderRepository.saveBookingOrder(bookingOrder); 
    
        System.out.println("Booking berhasil dilakukan dengan pembayaran cash.");
    }
    

private static void processCoinPayment(MemberCustomer memberCustomer, List<ItemService> selectedServices, int totalCost) {
    double memberCoinBalance = memberCustomer.getSaldoCoin();
    if (memberCoinBalance < totalCost) {
        System.out.println("Saldo Coin tidak mencukupi.");
        return;
    }

    BookingOrder bookingOrder = new BookingOrder();
    bookingOrder.setCustomer(memberCustomer);
    bookingOrder.setServices(selectedServices);
    bookingOrder.setPaymentMethod("Saldo Coin");
    bookingOrder.setTotalServicePrice(totalCost);
    bookingOrder.calculatePayment();

    BookingOrderRepository.addBookingOrder(bookingOrder);

    memberCustomer.setSaldoCoin(memberCoinBalance - totalCost);
    System.out.println("Booking berhasil dilakukan dengan pembayaran Saldo Coin. Saldo Coin tersisa: " + memberCustomer.getSaldoCoin());
}

private static String generateUniqueBookingId() {
    return "Book-" + System.currentTimeMillis();
}


    private static ItemService findServiceById(String itemId) {
        for (ItemService service : listAllItemService) {
            if (service.getItemId().equals(itemId)) {
                return service;
            }
        }
        return null;
    }

    public static void topUpSaldo(Customer loggedInCustomer) {
        if (loggedInCustomer instanceof MemberCustomer) {
            MemberCustomer memberCustomer = (MemberCustomer) loggedInCustomer;
            System.out.println("=== Top Up Saldo Coin ===");
            System.out.print("Masukkan jumlah saldo coin yang ingin ditambahkan: ");
            double saldoToAdd = input.nextDouble();
            input.nextLine(); 
            
            double currentSaldo = memberCustomer.getSaldoCoin();
            memberCustomer.setSaldoCoin(currentSaldo + saldoToAdd);
            
            System.out.println("Top up saldo coin berhasil. Saldo coin sekarang: " + memberCustomer.getSaldoCoin());
        } else {
            System.out.println("Maaf, fitur ini hanya untuk Member saja!");
        }
    }
    

    public static void informasiBookingOrder(Customer loggedInCustomer) {
        System.out.println("=== Informasi Booking ===");
        List<BookingOrder> bookingOrders = BookingOrderRepository.getBookingOrdersByCustomerId(loggedInCustomer.getCustomerId());
        PrintService.printInfoBooking(bookingOrders);
        if (bookingOrders.isEmpty()) {
            System.out.println("Tidak ada booking order yang ditemukan.");
            return;
        }
    }
    private static String formatServiceList(List<ItemService> services) {
        StringBuilder sb = new StringBuilder();
        for (ItemService service : services) {
            sb.append(service.getServiceName()).append(", ");
        }
        if (sb.length() > 0) {
            sb.setLength(sb.length() - 2);
        }
        return sb.toString();
    }
    
    
}
