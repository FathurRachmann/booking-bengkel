package com.bengkel.booking.services;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

import java.util.List;
import java.util.Scanner;

public class MenuService {
    private static List<Customer> listAllCustomers = CustomerRepository.getAllCustomer();
    private static List<ItemService> listAllItemService = ItemServiceRepository.getAllItemService();
    private static Scanner input = new Scanner(System.in);

    public static void run() {
        boolean isLooping = true;
        do {
            printStartMenu();
            int choice = Validation.validasiNumberWithRange("Masukan Pilihan Menu:", "Input Harus Berupa Angka!", "^[0-9]+$", 1, 0);
            switch (choice) {
                case 1:
                    login();
                    break;
                case 0:
                    System.out.println("Terima kasih telah menggunakan aplikasi Booking Bengkel.");
                    isLooping = false;
                    break;
                default:
                    System.out.println("Pilihan tidak valid. Silakan coba lagi.");
                    break;
            }
        } while (isLooping);
    }

    public static void login() {
        int loginAttempts = 0;
        final int MAX_LOGIN_ATTEMPTS = 3;

        while (loginAttempts < MAX_LOGIN_ATTEMPTS) {
            System.out.println("=== Login ===");
            System.out.print("Customer ID: ");
            String customerId = input.nextLine();
            System.out.print("Password: ");
            String password = input.nextLine();

            Customer customer = findCustomerById(customerId);
            if (customer != null) {
                if (customer.getPassword().equals(password)) {
                    System.out.println("Login successful!");
                    mainMenu(customer);
                    return; 
                } else {
                    System.out.println("Password yang Anda masukkan salah!");
                }
            } else {
                System.out.println("Customer ID tidak ditemukan atau salah!");
            }

            loginAttempts++;
            if (loginAttempts >= MAX_LOGIN_ATTEMPTS) {
                System.out.println("Anda telah melebihi batas maksimal percobaan login.");
                System.exit(0);
            }
        }
    }

    private static Customer findCustomerById(String customerId) {
        for (Customer customer : listAllCustomers) {
            if (customer.getCustomerId().equals(customerId)) {
                return customer;
            }
        }
        return null; 
    }

    static boolean backToMainMenu = false;
    public static void mainMenu(Customer loggedInCustomer) {
        if (loggedInCustomer == null) {
            System.out.println("Anda belum login.");
            return;
        }

        String[] listMenu = {"Informasi Customer", "Booking Bengkel", "Top Up Bengkel Coin", "Informasi Booking", "Logout"};
        int menuChoice = 0;
        boolean isLooping = true;

        do {
            PrintService.printMenu(listMenu, "Booking Bengkel Menu");
            menuChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu:", "Input Harus Berupa Angka!", "^[0-9]+$", listMenu.length - 1, 0);

            switch (menuChoice) {
                case 1:
                    BengkelService.informasiCustomer(loggedInCustomer);
                    break;
                case 2:
                    BengkelService.booking(loggedInCustomer);
                    break;
                case 3:
                    BengkelService.topUpSaldo(loggedInCustomer);
                    break;
                case 4:
                    BengkelService.informasiBookingOrder(loggedInCustomer);
                    break;
                case 0:
                System.out.println("Logout");
                isLooping = false;
                break;
        }
    } while (isLooping);
}

public static void printStartMenu() {
    System.out.println("Aplikasi Booking Bengkel");
    System.out.println("1. Login");
    System.out.println("0. Exit");
}
}
