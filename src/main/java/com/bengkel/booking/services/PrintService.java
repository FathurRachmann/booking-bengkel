package com.bengkel.booking.services;

import java.util.List;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Car;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.Vehicle;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class PrintService {
	
	public static void printMenu(String[] listMenu, String title) {
		String line = "+---------------------------------+";
		int number = 1;
		String formatTable = " %-2s. %-25s %n";
		
		System.out.printf("%-25s %n", title);
		System.out.println(line);
		
		for (String data : listMenu) {
			if (number < listMenu.length) {
				System.out.printf(formatTable, number, data);
			}else {
				System.out.printf(formatTable, 0, data);
			}
			number++;
		}
		System.out.println(line);
		System.out.println();
	}
	
	public static void printVechicle(List<Vehicle> listVehicle) {
		String formatTable = "| %-2s | %-15s | %-10s | %-15s | %-15s | %-5s | %-15s |%n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.format(line);
	    System.out.format(formatTable, "No", "Vechicle Id", "Warna", "Brand", "Transmisi", "Tahun", "Tipe Kendaraan");
	    System.out.format(line);
	    int number = 1;
	    String vehicleType = "";
	    for (Vehicle vehicle : listVehicle) {
	    	if (vehicle instanceof Car) {
				vehicleType = "Mobil";
			}else {
				vehicleType = "Motor";
			}
	    	System.out.format(formatTable, number, vehicle.getVehiclesId(), vehicle.getColor(), vehicle.getBrand(), vehicle.getTransmisionType(), vehicle.getYearRelease(), vehicleType);
	    	number++;
	    }
	    System.out.printf(line);
	}
	
    public static void printItemService(List<ItemService> listAllItemService, Vehicle vehicle) {
        System.out.println("Layanan yang tersedia untuk kendaraan:");
        String formatTable = "| %-2s | %-15s | %-20s | %-15s | %-15s |%n";
        String line = "+----+-----------------+---------------------+---------------------+---------------------+%n";
        System.out.format(line);
        System.out.format(formatTable, "ID", "Service Id", "Nama Service", "Tipe Kendaraan", "Harga");
        System.out.format(line);
        int number = 1;
        for (ItemService itemService : listAllItemService) {
            if (itemService.getVehicleType().equals(vehicle.getVehicleType())) {
                System.out.format(formatTable, number, itemService.getItemId(), itemService.getServiceName(), itemService.getVehicleType(),itemService.getPrice());
                number++;
            }
        }
        System.out.printf(line);
    }

    public static void printInfoBooking(List<BookingOrder> bookingOrders) {
        System.out.println("Informasi Booking:");
        String formatTable = "| %-15s | %-20s | %-15s | %-15s | %-20s | %-20s |%n";
        String line = "+-----------------+---------------------+-------------------+-------------------+---------------------+---------------------+%n";
        System.out.format(line);
        System.out.format(formatTable, "Booking Id", "Nama Customer", "Payment Method", "Total Service", "Total Payment", "List Service");
        System.out.format(line);
        for (BookingOrder bookingOrder : bookingOrders) {
            System.out.printf(formatTable, bookingOrder.getBookingId(), bookingOrder.getCustomerName(), bookingOrder.getPaymentMethod(), bookingOrder.getTotalServicePrice(), bookingOrder.getTotalPayment(), bookingOrder.getServices());
        }
        System.out.printf(line);
    }
    
    }
	

